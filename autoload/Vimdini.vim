if exists ('g:vimdini_loaded') || !has ('python')
    finish
endif

" Import vimdini so that we can use it
exec "python from vimdini import vimdini"

" Commands
"
command! -nargs=0 VimdiniBufferSend :call Vimdini#BufferSend()<c-r>
command! -nargs=1 VimdiniFileSend :call Vimdini#FileSend(<n-arg>)<c-r>

" Functions
"
function! Vimdini#BufferFile()
    exec "python vimdini.send_buffer_to_houdini()"
endfunction


function! Vimdini#SendFile(path)
    exec "python vimdini.send_file_to_houdini(" a:path ")"
endfunction


let g:vimdini_loaded = '1'
