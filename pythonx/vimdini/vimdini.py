#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Send a file to Houdini, using hyprc.

For more information on what hrpyc is, look at this documentation:

https://media.readthedocs.io/pdf/rpyc/latest/rpyc.pdf
sidefx.com/docs/houdini/hom/rpc.html

'''

# IMPORT STANDARD LIBRARIES
import subprocess
import tempfile
import logging
import imp
import os
import re

# IMPORT LOCAL LIBRARIES
from . import worker


LOGGER = logging.getLogger()


def is_hou_import_line(text):
    '''bool: If the given import line is importing Houdini.'''
    hou_import = re.compile('import\s+hou')
    return bool(hou_import.search(text))


def is_hyprc_imported(lines):
    '''bool: If the given Python import lines contains an import for hrpyc.'''
    lines = [line.strip() for line in lines]
    hrpyc_import = re.compile('import\s+hrpyc')

    for line in lines:
        if hrpyc_import.match(line):
            return True

    return False


def reformat_source_lines(lines):
    '''Remove "hou" and replace it with a hrpyc connection.

    Args:
        lines (list[str]): The lines to replace, presumably read from a file.

    Returns:
        list[str]: The adjusted lines.

    '''
    lines = list(lines)  # Make a copy

    for index, line in enumerate(lines):
        if is_hou_import_line(line):
            if not is_hyprc_imported(lines[:index]):
                lines.insert(index - 1, 'import hrpyc')
            lines[index] = 'connection, hou = hrpyc.import_remote_module()'
            break
    else:
        # If there were no lines importing Houdini at all, assume that the
        # user has Houdini modules and just insert hou, anyway
        #
        lines.insert(0, 'import hrpyc')
        lines.insert(1, 'connection, hou = hrpyc.import_remote_module()')

    return lines


def import_and_run_module(path):
    '''Try to import the given Python file and run it's "main()" function.'''
    module = imp.load_source('module', path)

    try:
        func = module.main
    except AttributeError:
        return

    func()


def send_lines_to_houdini(lines):
    '''Create a temporary file, based on the given file, and send it to Houdini.

    This function relies on Houdini's hrpyc module.

    Example:
        In Houdini, run this:
        >>> import hrpyc
        >>> hrpyc.start_server()

        Next, do this
        send_to_houdini(['print("Hello, World!")'])

        It should print "Hello, World!" in Houdini.

    Args:
        lines (lines[str]): The lines of commands to run.

    '''
    data = reformat_source_lines(lines)

    with tempfile.NamedTemporaryFile(delete=False) as file_:
        file_.writelines('\n'.join(data))

    LOGGER.info('Executing file: "{name}".'.format(name=file_.name))

    vim.command('call VimdiniSendFile("{name}")'.format(name=file_.name))
    importer = functools.partial(import_and_run_module, name=file_.name)
    worker.capture_and_send_to_vim(importer)


def send_buffer_to_houdini():
	'''Get a copy of the user's current buffer and send it to Houdini.

	This function requires Houdini to be listening for lines, using hrpyc.

	:seealso:`vimdini.send_lines_to_houdini`

	'''
	send_lines_to_houdini(list(vim.current.buffer))


def send_file_to_houdini(filename):
    '''Create a temporary file, based on the given file, and send it to Houdini.

    This function relies on Houdini's hrpyc module.

    Example:
        In Houdini, run this:
        >>> import hrpyc
        >>> hrpyc.start_server()

        Now write some script, like so:

        cat ~/temp/file.py
        ```
        hou.hipFile.load('TEST')
        ```

        Next, do this
        send_to_houdini('~/temp/file.py')

        Houdini will now try to import the "TEST" hip file. TEST doesn't exist
        so it should error in Houdini, but the point is you can now send text
        to Houdini automatically.

    Args:
        filename (str): The absolute path to some file to send to Houdini.

    '''
    filename = os.path.expanduser(filename)

    with open(filename, 'r') as file_:
        data = file_.readlines()

    send_lines_to_houdini(data)


def test():
    '''Test send to Houdini.'''
    filename = '~/temp/send.py'
    send_file_to_houdini(filename)


def test1():
    '''Test send to Houdini.'''
    lines = ['hou.hipFile.load("Whatever")']
    send_lines_to_houdini(lines)


test()
# test1()
