#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module that lets you run an object and write its output to disk, in Vim.

The stdout and stderr are captured and sent to log files and, if the any exception was raised while the object was being called, capture that traceback
and then add it to Vim's QuickFix window.

'''

# IMPORT STANDARD LIBRARIES
import traceback
import tempfile
import logging
import sys
import os
import re

# IMPORT THIRD-PARTY LIBRARIES
import wurlitzer
import vim

# IMPORT LOCAL LIBRARIES
import contextgrab


LOGGER = logging.getLogger('vimdini')
_SOURCE_COMPILE = re.compile('  File "(?P<path>.+)"')
_ITEM_COMPILE = re.compile(', in\s+(?P<name>.+)$')
_LINE_COMPILE = re.compile('line\s+(?P<number>\d+),')


def capture(func, exclude=True):
    '''Grab the stdout/stderr streams of the given function and and exceptions.

    Args:
        func (callable): Any function that takes no arguments.

    Returns:
        dict[str]:
            This creates 3 keys
            'stdout' (str): The path to where the stdout of this function saved.
            'stderr' (str): The path to where the stderr of this function saved.
            'traceback' (exception.BaseException): A raised exception, if any.

    '''
    trace = None
    with wurlitzer.pipes() as (out, err):
        try:
            func()
        except Exception:
            _, _, trace = sys.exc_info()

            if exclude:
                while True:
                    try:
                        next_trace = trace.tb_next
                    except AttributeError:
                        # TODO : Fill in a comment for this part, once done
                        pass

                    if next_trace:
                        trace = next_trace
                        break

    output = out.read()
    error = err.read()

    with tempfile.NamedTemporaryFile(delete=False, suffix='.log') as stdout:
        stdout.write(output)

    with tempfile.NamedTemporaryFile(delete=False, suffix='.log') as stderr:
        stderr.write(error)

    LOGGER.info('stdout saved to: "{stdout}".'.format(stdout=stdout.name))
    LOGGER.info('stderr saved to: "{stdout}".'.format(stdout=stderr.name))

    results = {
        'stdout': stdout.name,
        'stderr': stderr.name,
        'traceback': trace,
    }

    return results


def unpack_traceback(trace):
    '''Look through the traceback and get information about its exception.

    Args:
        trace (<traceback> or list[tuple[str, int, str, str]]):
            The traceback to parse or a list of entries in a traceback.
            If a traceback is given, its entries are found and used. If entries
            are given, they're processed, as-is.

    Returns:
        list[dict[str]]:
            'item' (str): The object / function / situation in the stack.
            'line' (int): The line number where the error occurred on.
            'source' (str): The location of where this error occurred, if found.
            'text': The description of the line.

    '''
    # If we get a Exception object, extract it. Otherwise, just assume that
    # the given object was traceback entries, already
    #
    try:
        entries = traceback.extract_tb(trace)
    except AttributeError:
        entries = trace

    output = []

    source_index = 0
    line_number_index = 1
    item_index = 2
    text_index = 3

    for entry in entries:
        item = entry[item_index]
        output.append({
            'item': entry[item_index],
            'line': entry[line_number_index],
            'source': entry[source_index],
            'text': entry[text_index],
        })

    return output


def make_quickfix_details(details):
    '''Change the given unpacked traceback data into a dict that Vim can use.

    Args:
        details (list[dict[str]]):
            'item' (str): The object / function / situation in the stack.
            'line' (int): The line number where the error occurred on.
            'source' (str): The location of where this error occurred, if found.
            'text': The description of the line.

    Returns:
        list[dict[str]]:
            'text' (str): The description to use.
            'lnum' (int): The line, starting from the number 1.
            'filename' (str): The absolute or relative path to a file, on-disk.

    '''
    output = []
    for item in details:
        text = item.get('text', '')
        output.append({
            'text': text.lstrip(),
            'lnum': item.get('line', 0),
            'filename': item.get('source', '')
        })

    return output


def add_to_quickfix(items, mode='add'):
    '''Add the given item(s) to the Vim quickfix window.'''
    modes = {
        'add': 'a',
        'a': 'a',
        'replace': 'r',
        'r': 'r',
    }

    try:
        mode = modes[mode]
    except KeyError:
        raise ValueError('Mode: "{mode}" was invalid. Options were, "{opt}".'
                         ''.format(mode=mode, opt=sorted(modes.keys())))

    vim.command("call setqflist({items}, '{mode}')".format(items=items, mode=mode))


def clear_quickfix():
    '''Empty out the contents of the current Vim session's Quickfix buffer.'''
    vim.command('cexpr []')


def capture_and_send_to_vim(func, clear=True):
    '''Run the given function and put its output into Vim's QuickFix buffer.

    Args:
        func (callable): The function to call.
        clear (:obj:`bool`, optional): If True, clear any QuickFix messages first.

    Reference:
        https://vi.stackexchange.com/questions/5110/quickfix-support-for-python-tracebacks

    '''
    results = capture(func)

    trace = results.get('traceback')
    stdout = results.get('stdout')
    stderr = results.get('stderr')

    config = [
        {
            'filename': stdout,
            'lnum': 1,
            'text': 'stdout file: "{stdout}"'.format(stdout=stdout),
        },
        {
            'filename': stderr,
            'lnum': 1,
            'text': 'stderr file: "{stderr}"'.format(stderr=stderr),
        }
    ]

    if clear:
        clear_quickfix()

    if trace:
        details = make_quickfix_details(unpack_traceback(trace))
        add_to_quickfix(details)

    add_to_quickfix(config)


def test():
    '''Run a test and see if it works.'''
    capture_and_send_to_vim(outer_function)


def outer_function():
    '''Print some stuff and then call a function that raises an error.'''
    print('RUNNING THIS FUNCTION')
    print('asdfsd,asdfasdfasdfasdfasfd')
    foo()


def foo():
    '''Raise an error.'''
    raise ValueError('information')


test()
